function [lat_sm,lon_sm,elev_sm,idx_keep] = reduce_to_unique_spatial_GPS_datapoints(lat,lon,elev,method)
%
% reduce arrays lat, lon, and elev to unique spatial data points
%
% lat=[1.1 1.1 2.2 3.3 4.4 4.1 5.5 6.6 7.1 8.8 8.8 8.8]; % 12 points (3 repeated lats) 
% lon=[1.1 2.1 2.2 3.1 4.4 4.4 5.1 6.1 7.7 8.8 8.8 8.9]; % 12 points (2 repeated lon) 
%
% method 1 uses only lat to reduce; result is 9 points  (3 lats are repeated) 
% method 2 uses both to reduce    ; result is 11 points (1 repeated location in both sets) 
% method 3 combines both with: (1E4)lat .* lon   then finds the unique, unsorted result 
% method 4 is distance based, keeping all successive points that exceed a 3D 
%          Euclidian distance threshold from the previous
%
% If in doubt, use method=4.
%
% To see why, see examples in: \Google_drive\Compere\technical\m\reduce_to_unique_spatial_GPS_datapoints
% which pulls from this csv: 2021_12_16__15_18_10_Quadcopter_reading_Spiderman_Rural5.csv
%
% Interesting reading references:
% - Cantor's pairing function for combining 2 numbers uniquely with forward and inverse mapping 
% - Matthew Szudzik, Wolfram Research: http://szudzik.com/ElegantPairing.pdf
% - both with other methods: https://stackoverflow.com/a/13871379/7621907
%
% Marc Compere, comperem@erau.edu
% created : 19 Aug 2020
% modified: 22 Dec 2021


if nargin<4
    method=4;   % 1->removed repeated longitudes only
                % 2->both points must be repeated to be removed
                % 3->both points must be repeated, but uses different algorithm 
                % 4->distance based thredhold ( BEST )
end
str=sprintf('using method=%i',method); disp(str)


if method==1
    [lat_sm,idx_keep]  = unique(lat,'stable'); % remove duplicates; do not sort
    lon_sm  = lon(idx_keep);
    elev_sm = lat(idx_keep);
elseif method==2
    [lat_unused,idx_lat_keep]  = unique(lat,'stable'); % remove duplicates; do not sort
    [lon_unused,idx_lon_keep]  = unique(lon,'stable'); % remove duplicates; do not sort
    idx_keep = union(   idx_lat_keep,   idx_lon_keep   ); % keep all unique points from both lat and lon 
    lat_sm  = lat(idx_keep);
    lon_sm  = lon(idx_keep);
    elev_sm = lat(idx_keep);
elseif method==3
    myVec = (1e4*lat) .* lon; % encode both with element-by-element product: [(scaleFactor)*lat] * lon
    [unused,idx_keep] = unique(myVec,'stable'); % find unique but do not sort

    % this is the minimim set of unique (X,Y) points
    lat_sm  = lat(idx_keep); % (m) minimum set of X-positions
    lon_sm  = lon(idx_keep); % (m) minimum set of Y-positions
    elev_sm = lat(idx_keep);
elseif method==4
    % Euclidian distance-based method with minimum threshold to remove redundant points
    distMin=0.001; % (m) if distance between two successive points (only) is less than this, then remove the point
    str=sprintf('distMin=%i(m)',distMin); disp(str)
    [X,Y,utmzone,utmhemi] = wgs2utm(lat,lon); % convert from (lat,lon) to (X,Y) in meters
    
    dX=diff(   X(:)); % (m)
    dY=diff(   Y(:)); % (m)
    dZ=diff(elev(:)); % (m)
    delta=sqrt(dX.^2 + dY.^2 + dZ.^2); % (m)
    
    idx_keep=find(delta>=distMin); % keep all successive distance changes above the 1mm threshold
    lat_sm  = lat(idx_keep);
    lon_sm  = lon(idx_keep);
    elev_sm = elev(idx_keep);
end






