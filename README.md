# Path Curvature Estimation from GPS Data Using Cubic Splines

This repository contains example Matlab post-processing scripts to:
  1. load a .csv file with GPS data
  2. remove duplicate GPS points
  3. fit cubic splines to the unique set of sequential GPS points
  4. estimate path curvature from the parametric plane-curve.

The cubic spline's are created using GPS waypoint index for their parameter. These splines, $`p_X(s)`$ and $`p_Y(s)`$, create a parametric plane curve, $`p(s)=[ p_X(s) , p_Y(s) ]`$, which is then used to estimate curvature along the path using $`p_X(s)`$ and $`p_Y(s)`$ 1st and 2nd derivatives.

## Powerpoint presentation
A [slide set with more detailed explanation](Race Line Data Postprocessing and Curvature Estimation.pdf) is included.

## Citations:
 1. The source GPS and sensor data is provided by Dr. Charles Tucker from a Mid-Ohio track lap in [MidOhioLap3.csv](MidOhioLap3.csv) from repository: https://github.com/charlestucker3/ARES-race-line-data.
 2. `plot_google_map.m` is open source code from Zohar Bar-Yehuda, [available Mathworks File Exchange](https://www.mathworks.com/matlabcentral/fileexchange/27627-zoharby-plot_google_map). If you plot more than a few times a month please get your own free API key from Google.
 3. `wgs2utm.m` is a WGS84 (lat,lon) coordinate convertion to UTM coordinates in meters written by Alexandre Schimel [avaialble on Mathworks File Exchange](https://www.mathworks.com/matlabcentral/fileexchange/14804-wgs2utm-version-2).


## The GPS points with a Google maps overlay plotted in Matlab:
![map plot](images/figure_51.png)


## Parametric cubic splines are created on X-waypoint axes and Y-waypoint axes:
![cubic splines](images/figure_101.png)


## Path curvature, $`\kappa`$ as a function of path waypoint number:
![curvature vs. waypoint](images/figure_104.png)


## Path curvature, $`\kappa`$ as a function of path distance:
![curvature vs. distance](images/figure_105.png)



***

Marc Compere, Phd, <comperem@erau.edu> <br>
https://sites.google.com/site/comperem <br>
created : 06 July 2022 <br>
modified: 14 July 2022