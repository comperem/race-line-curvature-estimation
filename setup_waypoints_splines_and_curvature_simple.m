% path geometry - starting with waypoints, create splines, display on a figure, 
%                 and compute curvature
%
% made simpler from the version taken from:
%     C:\Users\comperem\Google_drive\Compere\Courses\ME620_Adv_Veh_Dyn\m\simple_waypoint_spline_curvature_example.2020.06.16
%
% Marc Compere, comperem@erau.edu
% created : 03 Apr 2018
% modified: 10 Jul 2022

clear; clear all ; clear functions
%close('all')
format long g

addTextAtEachPoint          = 1; % (0/1) add waypoint numbers?
reduce_to_unique_GPS_points = 1; % (0/1) 0-->do nothing; 1-->downsample to remove repeated GPS points
makeFigureImages            = 0; % (0/1) write figures to a file?


path_sel=3;
if path_sel==1
    % path around hand-made track #1 after optimizing for distance and curvature
    % for details see trackmin() and pptx in: optim_path_geometry.2018.04.09.zip
    %load('path10.mat')
    load('path12.mat')
    nav_S(:,2)=-1.0*nav_S(:,2); % flip to achieve SAE Y-coordinates
elseif path_sel==2
    load('Task_1_Departure_candidate_path_1.kml.mat')
    nav_S(:,1)=X;
    nav_S(:,2)=Y;
elseif path_sel==3
    % from Chuck Tucker's MARS example discussed in Vehicle Dynamics Professionals 
    % here: https://www.facebook.com/groups/vdprofessionals/?multi_permalinks=741691543740532
    % and : https://github.com/charlestucker3/ARES-race-line-data
    lap3 = readtable('MidOhioLap3.csv');
    % lap3(1:4,:)
    
    t   = lap3.time;   % (s)
    lat = lap3.lat;    % (decimal degrees) latitude
    lon = lap3.lon;    % (decimal degrees) longitude
    elev= lap3.height; % (m) track elevation is ~1345ft which is 410m
    
    %% pull out unique vehicle information
    if reduce_to_unique_GPS_points==1
        [lat_sm,lon_sm,elev_sm,idx_keep] = reduce_to_unique_spatial_GPS_datapoints(lat,lon,elev);
        t_sm       = t(idx_keep);
        elev_sm    = elev(idx_keep);
        str=sprintf('reduced %i redundant GPS points to a unique set of %i points',length(lat),length(lat_sm)); disp(str)
    else
        lat_sm     = lat;
        lon_sm     = lon;
        t_sm       = t;
        elev_sm    = elev;
    end
    
    [X_raw,Y_raw,utmzone,utmhemi] = wgs2utm(lat_sm,lon_sm); % convert from (lat,lon) to (X,Y) in meters
    X_origin=X_raw(1); % (m) origin of UTM zone is a giant number
    Y_origin=Y_raw(1); % (m)
    Z_origin=min(elev_sm); % (m)
    X=X_raw-X_origin; % (m) remove giant UTM offset
    Y=Y_raw-Y_origin; % (m)
    Z=elev_sm-Z_origin;
    
    
    % make a 3D plot
    figure(50),clf
    plot3(X,Y,Z)
    view(-90,20)
    xlabel('UTM X-axis (m)')
    ylabel('UTM Y-axis (m)')
    zlabel('UTM Z-axis (m)')
    grid on
    view(170,40)
    
    nav_S(:,1)=X; % nav_S is the variable used for spline interpolation and curvature estimation 
    nav_S(:,2)=Y;
    
    
    % ----------------------------------------
    %      plot on google maps background
    % ----------------------------------------
    h=figure(51); clf
    set(gcf,'Name','GPS Lat-Lon map')
    hold on
    plot(lon_sm,lat_sm,'MarkerSize',12,'Marker','.','Color','cyan')
    plot_google_map('APIKey','AIzaSyCstEH_eMsiqIk5JemUjVvjG8THE4EYuno') % Compere's Google Maps API key
    %plot_google_map('MapType','satellite') % 'roadmap' 'hybrid' 'terrain'
    plot_google_map('MapType','hybrid','ShowLabels',0,'Scale',2) % 'roadmap' 'hybrid' 'terrain' 'satellite'

    % myXmax=-Inf; myXmin=+Inf; myYmax=-Inf; myYmin=+Inf;
    % for i=1:N
    %     if myXmax<max(lon{i}), myXmax=max(lon{i}); end
    %     if myXmin>min(lon{i}), myXmin=min(lon{i}); end
    %     if myYmax<max(lat{i}), myYmax=max(lat{i}); end
    %     if myYmin>min(lat{i}), myYmin=min(lat{i}); end
    % end
    % hax.XLim=[myXmin myXmax];
    %axis equal

    grid on
    xlabel('Longitude (deg)','FontSize',14)
    ylabel('Latitude (deg)','FontSize',14)
    %legend(str,'location','best')

    %addTextAtEachPoint=1;
    if addTextAtEachPoint==1
        %skipNth=5; % skip every N'th waypoint
        skipNth=20; % skip every N'th waypoint
        for j=1:skipNth:length(lon_sm)
            %str=sprintf('wapoint\n%i',i);
            %str=sprintf('i=%i\nv=%0.1f\n(mph)', i, GPS_speed_mph_sm(i) );
            str=num2str(j);
            text(lon_sm(j),lat_sm(j),str,'HorizontalAlignment','center','VerticalAlignment','bottom','Color','#EDB120','FontSize',10); % 'Color','white' 'black'
            % gold      : #EDB120
            % maroon    : #A2142F
            % light blue: #4DBEEE
        end
    end
    
    if makeFigureImages==1, hf=gcf; printStr=sprintf('print(%i,''%s_figure_%i.jpg'',''-djpeg'')',hf.Number,testName,hf.Number); eval(printStr), end
    
end


nav_Ns = length(nav_S);

% make a 2D plot
figure(100),clf
%plot(nav_S(:,1),nav_S(:,2),'bo-','LineWidth',6)
plot(nav_S(:,1),nav_S(:,2),'bo-')
grid on
hold on
if addTextAtEachPoint==1
    for i=1:skipNth:(nav_Ns-1)
        text(nav_S(i,1),nav_S(i,2),num2str(i),'HorizontalAlignment','Center','VerticalAlignment','Bottom','FontSize',14);
    end
end

grid on
xlabel('X-axis (m)')
ylabel('Y-axis (m)')
axis equal



% make splines for the entire path
spline_select=4; % 1:spline(), 2:pchip(), 3:makima()
% for N waypoints, spline generates (N-1) polynomials of 4 coefficients each 
% pp = spline(x,y)
if spline_select==1
    p_X = spline( [1:nav_Ns] , nav_S(:,1) ); % for N waypoints, spline generates
    p_Y = spline( [1:nav_Ns] , nav_S(:,2) ); % (N-1) polynomials of 4 coefficients each
    spline_str='spline()';
elseif spline_select==2
    p_X = pchip( [1:nav_Ns] , nav_S(:,1) ); % for N waypoints, spline generates
    p_Y = pchip( [1:nav_Ns] , nav_S(:,2) ); % (N-1) polynomials of 4 coefficients each
    spline_str='pchip()';
elseif spline_select==3
    % modified Akima's formula for smaller wigglies
    p_X = makima( [1:nav_Ns] , nav_S(:,1) ); % for N waypoints, spline generates
    p_Y = makima( [1:nav_Ns] , nav_S(:,2) ); % (N-1) polynomials of 4 coefficients each
    spline_str='makima()';
elseif spline_select==4
    % pp = csaps(x,y) *smoothing* spline, fundamentally different than spline/pchip/makima 
    % values = csaps(x,y,p,xx), smoothing parameter, p=1 is closest to data, p=0 is flat-line average 
    %p=0.9;
    %p=0.5;
    p=0.1; % this works surprisingly well by changing (lat,lon) locations towards greater smoothness 
    p_X = csaps(1:nav_Ns,X,p); % generate smoothing cubic spline in pp form
    p_Y = csaps(1:nav_Ns,Y,p);
    spline_str='csaps()';
end



% plot splines in parameter space
figure(101),clf
ds=0.1; % (%) sub-interval discretization length as a fraction on [0 1]
%ds=0.5;
X_spline = ppval(p_X,1:ds:(nav_Ns+1));
Y_spline = ppval(p_Y,1:ds:(nav_Ns+1));
subplot(2,1,1)
    plot(1:(nav_Ns+0),X,'r.','MarkerSize',8)
    hold on
    plot(1:ds:(nav_Ns+1),X_spline,'b.-')
    xlabel('waypoint number')
    ylabel('X-value (m)')
    grid on
    legend('unique XY points (m)','spline interpolant (m)','location','southeast')
subplot(2,1,2)
    plot(1:(nav_Ns+0),Y,'r.','MarkerSize',8)
    hold on
    plot(1:ds:(nav_Ns+1),Y_spline,'b.-')
    xlabel('waypoint number')
    ylabel('Y-value (m)')
    grid on
    legend('unique XY points (m)','spline interpolant (m)','location','southeast')


% compare unique XY points and interpolated points
figure(102),clf
hold on
plot(nav_S(:,1),nav_S(:,2),'r.','MarkerSize',12) % (m) unique XY points from GPS data
plot(X_spline,Y_spline,'b.')     % (m) interpolated XY points
grid on
if addTextAtEachPoint==1
    for i=1:skipNth:nav_Ns
        text(nav_S(i,1),nav_S(i,2)+15,num2str(i),'HorizontalAlignment','Center','VerticalAlignment','Middle','FontSize',14);
    end
end
xlabel('X-axis (m)')
ylabel('Y-axis (m)')
axis equal



% plot cubic splines between every waypoint with different colors
figure(103),clf
hold on
%cmap=colormap(jet(nav_Ns));
cmap=colormap(lines(nav_Ns));

kappa_full  = [];
domain_full = [];
length_full = []; L_base=0;
for i=1:(nav_Ns-1) % step through each spline just created
    
    domain=[i:ds:(i+1)];
    xx = ppval(p_X,domain); % evaluation along the i'th polynomial
    yy = ppval(p_Y,domain);
    plot(xx,yy,'.-','Color',cmap(mod(i,length(cmap)),:));
    
    [kappa_out,domain_out , p_X_deriv,p_Y_deriv] = computeCurvature(i, p_X, p_Y, ds );
    N=length(domain)-1; % estimate arc length with curvature discretization 
    [L,L_cumu] = computePolyLength( p_X , p_Y , [domain(1) domain(end)], N );
    
    kappa_out = -kappa_out; % sign change: RH-turn is (+) curvature
    domain_full = [domain_full, domain_out(2:end)    ];
    kappa_full  = [kappa_full , kappa_out(2:end)     ];
    length_full = [length_full, L_base+L_cumu(2:end) ];
    L_base = L_base + L;
    
end

%plot(nav_S(:,1),nav_S(:,2),'bo')
grid on
if addTextAtEachPoint==1
    for i=1:skipNth:nav_Ns
        text(nav_S(i,1),nav_S(i,2)+15,num2str(i),'HorizontalAlignment','Center','VerticalAlignment','Middle','FontSize',14);
    end
end
xlabel('X-axis (m)')
ylabel('Y-axis (m)')
axis equal


% plot curvature as a function of waypoint number
figure(104),clf
plot(domain_full,kappa_full,'.-')
grid on
xlabel('Waypoint number')
ylabel('Curvature,\kappa (1/m)')
%hax=gca; % get current axes
%hax.XTick=[1:1:nav_Ns]; % improve tick marks
ylim([-0.04 +0.04])



% plot curvature as a function of distance along the track
figure(105),clf
plot(length_full,kappa_full,'.')
grid on
xlabel('path length (m)')
ylabel('Curvature,\kappa (1/m)')
%hax=gca; % get current axes
%hax.XTick=[1:1:nav_Ns]; % improve tick marks
ylim([-0.04 +0.04])
xlim([0 3600])


% compute spling polynomial length
N=100;
[L,L_cumu] = computePolyLength( p_X , p_Y , [1 nav_Ns], N );

fprintf('\npath length %0.2f(km)\n',L/1000);





