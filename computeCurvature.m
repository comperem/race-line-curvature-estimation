function [kappa,domain,p_X_deriv,p_Y_deriv] = computeCurvature(i,p_X,p_Y,ds)

% ----------------------------------------------------------------------
% Continuous curvature over the entire spline is a G2 continuity test.
% ----------------------------------------------------------------------
%
% this function computes curvature of a parameterized space curve,
%
%       P(s) = P( p_X(s) , p_Y(s) )
%
% where p_X(s) and p_Y(s) are spline polynomials as a function of s.
%
% The parameter s spans [0 1] and allows expression of the X and Y
% polynomial p_X(s) and p_Y(s) along the interior of any two
% waypoints on [ (i)  (i+1) ].
%
% Curvature, kappa, is defined from the curvature formula for a
% parametricallly defined curve in space as a function of 2 variables.
%
% Curvature, kappa(s), is the reciprocal of the instantaneous radius of
% curvature, R(s).
%
% For more, see wiki on space curves:
%    https://en.wikipedia.org/wiki/Curvature#Space_curves
%    https://mathworld.wolfram.com/RadiusofCurvature.html
%
% Marc Compere, comperem@gmail.com
% created : 07 Jul 2016
% modified: 20 Jun 2020

if nargin<4
    ds=0.1;
end

        % construct the p_X polynomial objects for this waypoint segment 
        CX1=p_X.coefs(i,1);
        CX2=p_X.coefs(i,2);
        CX3=p_X.coefs(i,3);
        pp_p_X_deriv       = mkpp([i (i+1)],[  3*CX1   2*CX2   1*CX3   ]);
        pp_p_X_deriv_deriv = mkpp([i (i+1)],[  6*CX1   2*CX2           ]);
        
        % construct the p_Y polynomial objects for this waypoint segment 
        CY1=p_Y.coefs(i,1);
        CY2=p_Y.coefs(i,2);
        CY3=p_Y.coefs(i,3);
        pp_p_Y_deriv       = mkpp([i (i+1)],[  3*CY1   2*CY2   1*CY3   ]);
        pp_p_Y_deriv_deriv = mkpp([i (i+1)],[  6*CY1   2*CY2           ]);
        
        
        % evaluate both X and Y polynomial derivatives on [(i) to (i+1)], 
        % which is identical to evaluating along s=[0 1]
        domain = [i:ds:(i+1)];
        p_X_deriv = ppval(pp_p_X_deriv,domain);
        p_Y_deriv = ppval(pp_p_Y_deriv,domain);
        
        p_X_deriv_deriv = ppval(pp_p_X_deriv_deriv,domain);
        p_Y_deriv_deriv = ppval(pp_p_Y_deriv_deriv,domain);
        
        % compute curvature along this interval, [(i) (i+1)]
        num = (p_Y_deriv_deriv .* p_X_deriv) -  (p_X_deriv_deriv .* p_Y_deriv);
        den = (p_X_deriv).^2  +  (p_Y_deriv).^2;
        
        kappa = (num) ./ (den).^(3/2); % (1/m) curvature is 1/R(s)







